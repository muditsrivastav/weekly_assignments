def call (int buildNumber) {
  //batchFile 'echo buildNumber'
    pipeline {
      agent any
      
      triggers {
        cron('H/4 * * * *')
      }
      
      stages {
        stage('Buid') {
          steps {
            echo "The build Number is ${buildNumber}"
          }
        }
      }
      
      post {
        always {
          emailext (
            subject: "job '${env.JOB_NAME} ${env.BUILD_NUMBER}'",
            body: """<p>Check console output at <a href="${env.BUILD_URL}">${env.JOB_NAME}</a></p>""",
            to: "2015pcecsmudit@poornima.org",
            from: "2015pcecsmudit@poornima.org"
            )
        }
      }
    
  }
}
